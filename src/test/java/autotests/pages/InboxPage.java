package autotests.pages;

import autotests.utils.Constants;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;


import java.util.List;

/**
 * PageObject страницы сообщений
 */
public class InboxPage {

    @FindBy(css = ".yandex-header__logo")
    public WebElement logoLink;

    @FindBy(css = "div[data-tooltip='Входящие'] a")
    public WebElement incomeMailboxButton;

    @FindBy(css = ".yandex-header__search .search-input__text-bubble-container input")
    public WebElement searchField;

    @FindBy(css = ".mail-ComposeButton")
    public WebElement createNewMessageButton;

    @FindBy(css = ".ComposeRecipients .MultipleAddressesDesktop.ComposeRecipients-ToField div[is='x-bubbles']")
    public WebElement messageRecipientField;

    @FindBy(css = ".ComposeSubject-Content input")
    public WebElement messageTitleField;

    @FindBy(css = ".composeReact__message-body div[role='textbox']")
    public WebElement messageBodyField;

    @FindBy(css = ".ComposePopup-BodyContent .composeReact__footer .ComposeControlPanel-Part .ComposeSendButton button")
    public WebElement sendMessageButton;

    @FindBy(css = ".ComposeDoneScreen-Title")
    public WebElement messageHaveBeenSendTittle;

    private WebDriver driver;
    private Wait<WebDriver> wait;
    private final String MAILS_CSS = ".ns-view-messages-item-box span[title='%s']";

    public InboxPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT).withMessage(Constants.NOT_FOUND_MESSAGE);
    }

    public Boolean isLogoPresent(){
        wait.until(ExpectedConditions.visibilityOf(logoLink));
        return logoLink.isDisplayed();
    }

    @Step("Посчитать количество вхоллящих сообщений по определенной почте")
    public Integer getReceivedMailsCountByMail(String mail){
        String receivedMessagesXpath = String.format(MAILS_CSS, mail);
        By byXpath = By.cssSelector(receivedMessagesXpath);
        wait.until(ExpectedConditions.visibilityOfAllElements((driver.findElements(byXpath))));
        List<WebElement> mails = driver.findElements(byXpath);

        return  mails.size();
    }

    @Step("Найти сообщения по тексту")
    public void setTextToSearchField(String text){
        wait.until(ExpectedConditions.visibilityOf((searchField)));
        searchField.sendKeys(text);
        searchField.sendKeys(Keys.ENTER);
    }

    @Step("Нажать кнопку написать")
    public void sendNewMessageButtonClick(){
        createNewMessageButton.click();
    }

    @Step("Отправить новое сообщение")
    public void createNewMessage(String messageRecipient, String messageTitle, String messageBody ) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf((messageRecipientField)));
        messageRecipientField.click();
        Thread.sleep(5000);

        String script = "arguments[0].innerHTML='%s'" ;
        ((JavascriptExecutor) driver).executeScript(String.format(script,messageRecipient), messageRecipientField);

        messageTitleField.click();
        messageTitleField.sendKeys(messageTitle);
        messageBodyField.click();
        messageBodyField.sendKeys(messageBody);
        sendMessageButton.click();
    }

    public boolean isMessageSent(){
        wait.until(ExpectedConditions.visibilityOf(messageHaveBeenSendTittle));
        return messageHaveBeenSendTittle.isDisplayed();
    }
}
