package autotests.pages;

import autotests.utils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.concurrent.TimeUnit;

public class YandexMailMainPage {


    @FindBy(xpath = "//div[@class='h-c-header__bar']//div[@class='h-c-header__cta']//ul//li[2]//a[@ga-event-action='sign in']")
    public WebElement enterToMailBox;

    private final String PROFILE_LINK = "div[data-identifier='%s']";

    private WebDriver driver;
    private Wait<WebDriver> wait;


    public YandexMailMainPage(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(2,TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT).withMessage(Constants.NOT_FOUND_MESSAGE);
    }

    @Step("Нажать кнопку Войти")
    public void enterToMailBoxButtonClick(){
        wait.until(ExpectedConditions.elementToBeClickable(enterToMailBox));
        enterToMailBox.click();
    }

    @Step("Нажать на профиль")
    public void clickProfileByMail(String mail){

        By profile = By.cssSelector(String.format(PROFILE_LINK,mail));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(profile)));
        WebElement profileElement = driver.findElement(profile);

        profileElement.click();
    }
}
