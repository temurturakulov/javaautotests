package autotests.pages;


import autotests.utils.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;


/**
 * PageObject страницы авторизации
 */
public class AuthorizationPage {

    @FindBy(css = "input[name='login']")
    public WebElement loginInput;

    @FindBy(id = "passp-field-passwd")
    public WebElement passwordInput;

    @FindBy(css = ".passp-button button[type='submit']")
    public WebElement nextStepButton;

    @FindBy(css = "div[data-t='phone_actual_skip'] button")
    public WebElement notNowButton;

    private WebDriver driver;
    private Wait<WebDriver> wait;

    public AuthorizationPage(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Constants.TIMEOUT).withMessage(Constants.NOT_FOUND_MESSAGE);
    }

    @Step("Заполнить поле ввода пароля")
    public void setPassword(String password){
        wait.until(ExpectedConditions.visibilityOf(passwordInput));
        passwordInput.sendKeys(password);
    }

    @Step("Заполнить поле логин")
    public void setLogin(String login){
        wait.until(ExpectedConditions.visibilityOf(loginInput));
        loginInput.sendKeys(login);
    }

    @Step("Нажать кнопку далее после ввода логина")
    public void pressNextStepButtonAfterLogin(){
        wait.until(ExpectedConditions.elementToBeClickable(nextStepButton));
        nextStepButton.click();
    }

    @Step("Нажать на кнопку далее после ввода пароля")
    public void pressNextStepButtonAfterPassword() {
        wait.until(ExpectedConditions.elementToBeClickable(nextStepButton));
        nextStepButton.click();
    }

    @Step("Нажать кнопку 'Не сейчас'")
    public void notNowButtonClick(){
        wait.until(ExpectedConditions.elementToBeClickable(notNowButton));
        notNowButton.click();
    }

}
