package autotests.smoke;

import autotests.pages.AuthorizationPage;
import autotests.pages.InboxPage;
import autotests.utils.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

import java.util.concurrent.TimeUnit;

//@Feature("Gmail tests")
public class MailTests {

    private WebDriver driver;

    @BeforeMethod
    @Parameters({"hostname", "hubAddress"})
    void setup(String hostname, String hubAddress) {
        try {
            driver = DriverFactory.create(hubAddress);
            driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(5,TimeUnit.SECONDS);
            driver.get(hostname);
            driver.manage().window().maximize();
        } catch (Exception e) {
            System.out.println("Failed to connect with " + hubAddress + "\n" + e.getMessage());
        }
    }

    @Description("Authorization test")
    @Parameters({"login","password"})
    @Test
    public void authorizationTest(String login, String password) throws InterruptedException {
        AuthorizationPage authorizationPage = new AuthorizationPage(driver);
        authorizationPage.setLogin(login);
        authorizationPage.pressNextStepButtonAfterLogin();
        authorizationPage.setPassword(password);
        authorizationPage.pressNextStepButtonAfterPassword();
        InboxPage inboxPage = new InboxPage(driver);

        Assert.assertTrue(inboxPage.isLogoPresent());
    }

    @Description("Send email test")
    @Parameters({"login","password","senderMail"})
    @Test
    public void sendMessageTest(String login, String password, String senderMail) throws InterruptedException {
        AuthorizationPage authorizationPage = new AuthorizationPage(driver);
        authorizationPage.setLogin(login);
        authorizationPage.pressNextStepButtonAfterLogin();
        authorizationPage.setPassword(password);
        authorizationPage.pressNextStepButtonAfterPassword();
        InboxPage inboxPage = new InboxPage(driver);

        inboxPage.setTextToSearchField(senderMail);
        Thread.sleep(10000);
        int messagesCount = inboxPage.getReceivedMailsCountByMail(senderMail);
        inboxPage.sendNewMessageButtonClick();
        String messageBody = "Hi! You have been sent "+ messagesCount+ " messages for  this account ";
        inboxPage.createNewMessage(senderMail,"Selenium test message", messageBody);

        Assert.assertTrue(inboxPage.isMessageSent());
    }

    @AfterMethod
    public void tearDown() {
        if(driver != null){
            driver.quit();
        }
    }

}
