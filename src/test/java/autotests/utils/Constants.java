package autotests.utils;

/**
 * Класс для хранения констант
 */
public final class Constants {

    public static final String NOT_FOUND_MESSAGE = "Element wasn't found.";

    public static final int TIMEOUT = 60;

    private Constants() {

    }

}
